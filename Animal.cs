﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    abstract class Animal : IMovement
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public double Weight { get; set; }
        public char Gender { get; set; }

        public Animal(string name, int age, double weight, char gender)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Gender = gender;
        }

        public void Eat()
        {
            if (Gender == 'm')
                Console.WriteLine($"Ouu, looks like {Name} is very hungry. Now he happly eats his meal.");
            else if(Gender == 'f')
                Console.WriteLine($"Ouu, looks like {Name} is very hungry. Now she happly eats her meal.");
        }

        public abstract void movementType();

        public abstract void move(int meters);

        public abstract string toString();
    }
}
