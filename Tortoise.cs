﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Tortoise : Animal
    {
        public string Specie { get; set; }
        public string Continent { get; set; }

        public Tortoise(string name, int age, double weight, char gender)
            : base(name, age, weight, gender)
        {
            Specie = "giant Galapagos tortoise";
            Continent = "South Africa";
        }

        public override void movementType()
        {
            Console.WriteLine($"{Name} can both walk and run.");
        }

        public override void move(int meters)
        {
            TimeSpan time = TimeSpan.FromSeconds(meters / (0.27 * 0.277777778));

            Console.WriteLine($"{Name} ran {meters} meters in {time.ToString(@"hh\:mm\:ss")}");
        }

        public override string toString()
        {
            if (Gender == 'm')
                return $"Meet our friend {Name}! He is a {Age} year old {Specie} that can mostly be found in {Continent}. He weights {Weight} lbs.";
            else
                return $"Meet our friend {Name}! She is a {Age} year old {Specie} that can mostly be found in {Continent}. She weights {Weight} lbs.";
        }
    }
}
