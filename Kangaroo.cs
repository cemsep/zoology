﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Kangaroo : Animal
    {
        public string Specie { get; set; }
        public string Continent { get; set; }

        public Kangaroo(string name, int age, double weight, char gender)
            : base(name, age, weight, gender)
        {
            Specie = "kangaroo";
            Continent = "Australia";
        }

        public override void movementType()
        {
            Console.WriteLine($"{Name} can only hop.");
        }

        public override void move(int meters)
        {
            TimeSpan time = TimeSpan.FromSeconds(meters / (35 * 0.277777778));

            Console.WriteLine($"{Name} hopped {meters} meters in {time.ToString(@"hh\:mm\:ss")}");
        }

        public override string toString()
        {
            if (Gender == 'm')
                return $"Meet our friend {Name}! He is a {Age} year old {Specie} that can mostly be found in {Continent}. He weights {Weight} lbs.";
            else
                return $"Meet our friend {Name}! She is a {Age} year old {Specie} that can mostly be found in {Continent}. She weights {Weight} lbs.";
        }
    }
}
