﻿using System;
using System.Collections.Generic;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            static void initializeAnimals(List<Animal> animals)
            {
                Tiger tiger = new Tiger("Tigey", 4, 400, 'm');
                Kangaroo kangaroo = new Kangaroo("Marilyn Monroo", 6, 200, 'f');
                Elephant elephant = new Elephant("Manny", 22, 6700, 'f');
                Tortoise tortoise = new Tortoise("Jack", 150, 475, 'm');

                animals.Add(tiger);
                animals.Add(kangaroo);
                animals.Add(elephant);
                animals.Add(tortoise);
            }

            static void displayInformation(List<Animal> animals)
            {
                foreach (Animal animal in animals)
                {
                    Console.WriteLine(animal.toString());
                    animal.movementType();
                    animal.move(200);
                    Console.Write("\n");
                }
            }

            initializeAnimals(animals);
            Console.WriteLine("======WELCOME TO ZOOLOGY======");
            Console.Write("\n");
            displayInformation(animals);
        }
    }
}
